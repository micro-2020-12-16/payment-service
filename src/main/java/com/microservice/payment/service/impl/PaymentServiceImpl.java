package com.microservice.payment.service.impl;

import com.microservice.payment.entity.Payment;
import com.microservice.payment.exception.NotFoundException;
import com.microservice.payment.repository.PaymentRepository;
import com.microservice.payment.request.CreatePaymentRequest;
import com.microservice.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired PaymentRepository paymentRepository;

    @Override
    public Payment createPayment(CreatePaymentRequest createPaymentRequest) {
        Payment payment = new Payment();
        payment.setInvoice(UUID.randomUUID().toString());
        payment.setOrderId(createPaymentRequest.getOrderId());
        payment.setAmount(createPaymentRequest.getAmount());
        payment.setCreatedAt(new Date());
        payment.setUpdatedAt(new Date());

        paymentRepository.save(payment);

        return payment;
    }

    @Override
    public Payment getPaymentById(String id) {
        Payment payment = findPaymentByIdOrThrowNotFound(id);

        return payment;
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    private Payment findPaymentByIdOrThrowNotFound(String id) {
        Payment payment = paymentRepository.findById(id).orElse(null);

        if (payment == null){
            throw new NotFoundException();
        } else {
            return payment;
        }
    }
}
