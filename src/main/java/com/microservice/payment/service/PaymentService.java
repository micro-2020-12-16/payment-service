package com.microservice.payment.service;

import com.microservice.payment.entity.Payment;
import com.microservice.payment.request.CreatePaymentRequest;

import java.util.List;

public interface PaymentService {

    public Payment createPayment(CreatePaymentRequest createPaymentRequest);

    public Payment getPaymentById(String id);

    public List<Payment> getAllPayments();
}
