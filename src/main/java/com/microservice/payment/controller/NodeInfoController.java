package com.microservice.payment.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class NodeInfoController {

    @Autowired
    ObjectMapper objectMapper;

    @GetMapping("/api/nodeinfo")
    public Map<String, Object> nodeInfo(HttpServletRequest request) throws JsonProcessingException {
        Map<String, Object> info = new HashMap<>();
        info.put("Local IP", request.getLocalAddr());
        info.put("Local Port", request.getLocalPort());

        return info;
    }
}
