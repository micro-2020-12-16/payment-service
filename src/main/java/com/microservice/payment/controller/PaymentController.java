package com.microservice.payment.controller;

import com.microservice.payment.entity.Payment;
import com.microservice.payment.request.CreatePaymentRequest;
import com.microservice.payment.response.ApiResponse;
import com.microservice.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PaymentController {

    @Autowired PaymentService paymentService;

    @PostMapping(
            value = {"/payment"},
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<ApiResponse> createOrder(@Valid @RequestBody CreatePaymentRequest request) {
        Payment payment = paymentService.createPayment(request);
        ApiResponse<Payment> apiResponse = new ApiResponse<Payment>(201, true, payment);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.CREATED);
    }

    @GetMapping(
            value = {"/payment/{id}"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getPaymentById(@PathVariable("id") String id) {
        Payment payment = paymentService.getPaymentById(id);
        ApiResponse<Payment> apiResponse = new ApiResponse<Payment>(200, true, payment);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @GetMapping(
            value = {"/payment"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getAllPayments() {
        List<Payment> payments = paymentService.getAllPayments();
        ApiResponse<Object> apiResponse = new ApiResponse<Object>(200, true, payments);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }
}
